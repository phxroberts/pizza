﻿using System;
using System.Linq;
using System.Threading.Tasks;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Newtonsoft.Json;


namespace PizzaCommon
{
	public class PizzaOrders
	{
		public static async Task<List<PizzaTypeOrders>> LoadAsync(int orderCount)
		{
			try {
				using (var reader = new StreamReader(typeof(PizzaOrders).GetTypeInfo().Assembly.GetManifestResourceStream ("PizzaCommon.pizzas.json")))
				{
					//Deserialize the orders
					List<PizzaType> pizzaOrders = JsonConvert.DeserializeObject<List<PizzaType>>(await reader.ReadToEndAsync().ConfigureAwait(false));
					//Query for the top orders
					var ordersBySize = (from p in pizzaOrders
						group p by p.Id into pizzaGrouping
						orderby pizzaGrouping.Count() ascending //descending
						select pizzaGrouping).Take(orderCount);
					//Summarize the orders
					var ordersSummary = (from pizzaOrderSummary in
						ordersBySize
						select new PizzaTypeOrders {
							Orders=pizzaOrderSummary.Count(),
							PizzaType=pizzaOrderSummary.ElementAt(0)}
					).ToList();
					return ordersSummary;
				}
			}
			catch(Exception x) {
				//Log the exception
			}
			return null;
		}
	}
	//Where a pizza is an ordered list of toppings
	public class PizzaType {
		private string[] m_Toppings = null;
		public string [] Toppings { 
			get { return m_Toppings; } 
			set { 
				m_Toppings = value;
				Array.Sort (m_Toppings);
				Id = String.Join ("", m_Toppings).GetHashCode ();
			}
		}
		public int Id { get; private set; }
	}
	public class PizzaTypeOrders {
		public int Orders { get; set; }
		public PizzaType PizzaType { get; set; }
	}
}

