﻿using System;
using System.Linq;
using System.Collections.Generic;

using Android.App;
using Android.Content;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;

//using PizzaCommon;
using System.IO;

namespace PizzaDroid
{
	[Activity (Label = "PizzaDroid", MainLauncher = true, Icon = "@drawable/icon")]
	public class MainActivity : Activity
	{
		protected async override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);

			// Set our view from the "main" layout resource
			SetContentView (Resource.Layout.Main);

			ListView topTwentyList = FindViewById<ListView> (Resource.Id.topTwentyList);
			//topTwentyList.Adapter = new PizzaOrderAdapter (this, await PizzaOrders.LoadAsync (20));
		}
	}

	/*
	class PizzaOrderAdapter : BaseAdapter<PizzaTypeOrders> {
		class PizzaOrderAdapterViewHolder : Java.Lang.Object {
			public TextView ToppingsView { get; set; }
			public TextView OrderCount { get; set; }
		}
		private Context m_Context = null;
		private List<PizzaTypeOrders> m_PizzaOrders = null;

		public PizzaOrderAdapter(Context c, List<PizzaTypeOrders> pizzaOrders) {
			m_Context = c;
			m_PizzaOrders = pizzaOrders;
		}

		#region implemented abstract members of BaseAdapter

		public override long GetItemId (int position)
		{
			return position;
		}

		public override View GetView (int position, View convertView, ViewGroup parent)
		{
			View v = convertView;
			if (v == null) {
				v = LayoutInflater.FromContext (m_Context).Inflate (Resource.Layout.pizza_orders_row,parent,false);
				v.Tag = new PizzaOrderAdapterViewHolder {
					ToppingsView = v.FindViewById<TextView> (Resource.Id.pizzaToppings),
					OrderCount = v.FindViewById<TextView> (Resource.Id.orderCount)
				};
			}

			PizzaTypeOrders pizzaOrders = m_PizzaOrders [position];
			PizzaOrderAdapterViewHolder viewHolder = v.Tag as PizzaOrderAdapterViewHolder;
			viewHolder.OrderCount.Text = "Orders: " + pizzaOrders.Orders.ToString();

			viewHolder.ToppingsView.SetLines (pizzaOrders.PizzaType.Toppings.Count());
			foreach(string pizzaTopping in pizzaOrders.PizzaType.Toppings) {
				viewHolder.ToppingsView.Text += pizzaTopping + "\n";
			}

			return v;
		}

		public override int Count {
			get {
				return m_PizzaOrders.Count;
			}
		}

		#endregion

		#region implemented abstract members of BaseAdapter

		public override PizzaTypeOrders this [int index] {
			get {
				return m_PizzaOrders [index];
			}
		}

		#endregion


	}
	*/
}


