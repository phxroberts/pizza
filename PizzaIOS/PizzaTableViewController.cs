using System;
using System.Linq;
using UIKit;
using Foundation;
using CoreGraphics;
using PizzaCommon;
using System.Collections.Generic;

namespace PizzaIOS
{
	partial class PizzaTableViewController : UITableViewController
	{
		private const string m_sCellId = "pizzacellid";
		private List<PizzaTypeOrders> m_PizzaOrders = new List<PizzaTypeOrders>();

		public PizzaTableViewController (IntPtr handle) : base (handle)
		{
			this.TableView.ContentInset = new UIEdgeInsets (20, 0, 0, 0);
			TableView.RowHeight = UITableView.AutomaticDimension;
			TableView.EstimatedRowHeight = 44.0f;			
		}
		public async override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			UpdateRows (await PizzaOrders.LoadAsync (1120));
		}
		public override UITableViewCell GetCell (UITableView tableView, NSIndexPath indexPath)
		{
			UITableViewCell cell = tableView.DequeueReusableCell (m_sCellId); //Storyboad source (always non-null)
			cell.TextLabel.LineBreakMode = UILineBreakMode.WordWrap;
			cell.TextLabel.Lines = 0;
			PizzaTypeOrders pizzaTypeOrders = m_PizzaOrders [indexPath.Row];
			cell.TextLabel.Text = "";
			foreach(string pizzaTopping in pizzaTypeOrders.PizzaType.Toppings) {
                cell.TextLabel.Text += pizzaTopping + " ";    // + "\n";
			}
			cell.DetailTextLabel.Text = pizzaTypeOrders.Orders.ToString();
			return cell;
		}

		public override nint RowsInSection (UITableView tableView, nint section)
		{
			return m_PizzaOrders.Count;
		}

		public void UpdateRows(List<PizzaTypeOrders> pizzaOrdersData) {
			//Replace all rows and refresh table
			m_PizzaOrders = pizzaOrdersData;
			TableView.ReloadData ();
		}
	}
}
